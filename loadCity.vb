Option Strict Off
Option Explicit On
Module loadCity
	
	Public g_sCities() As String
	Public g_iCities As Short
	
	Public Function loadCityTbl(ByRef sCnty As String) As Short
		Dim sTmp As String
		Dim sCityTbl As String
		Dim fh As Short
		Dim iCnt As Short
		Dim Index As Short
		Dim sCity As String
		
		On Error GoTo loadCityTbl_Err
		Erase g_sCities
		fh = FreeFile
		sCityTbl = g_sCityPath & "\" & sCnty & "City.dat"
		FileOpen(fh, sCityTbl, OpenMode.Input)
		sTmp = LineInput(fh)
		
		g_iCities = CShort(sTmp)
		ReDim g_sCities(g_iCities)
		
		iCnt = 0
		'First entry is normally blank
		Do While Not EOF(fh)
			sTmp = LineInput(fh)
			If sTmp <> "" Then
				g_sCities(iCnt) = sTmp
			Else
				g_sCities(iCnt) = " "
			End If
			iCnt = iCnt + 1
			If iCnt > g_iCities Then
				MsgBox("Bad city table.  Please check " & sCityTbl)
			End If
		Loop 
		FileClose(fh)
		fh = 0
		
		If iCnt < g_iCities Then
			MsgBox("City table count is unmatched.  Please check " & sCityTbl)
			loadCityTbl = 0
		Else
			loadCityTbl = 1
		End If
		
		Exit Function
		
loadCityTbl_Err: 
		If fh <> 0 Then FileClose(fh)
		MsgBox("Error loading city table " & sCityTbl)
		loadCityTbl = 0
	End Function
	
	'Check to see if the city is in the county
	Public Function isValidCity(ByRef sCity As String) As Boolean
		Dim iCnt As Short
		
		isValidCity = False
		For iCnt = 1 To g_iCities - 1
			If sCity = g_sCities(iCnt) Then
				isValidCity = True
				Exit For
			End If
		Next 
	End Function
	
	Public Function getCityName(ByRef iCityCode As Short) As String
		If iCityCode > 0 And iCityCode <= g_iCities Then
			getCityName = g_sCities(iCityCode)
		Else
			getCityName = ""
		End If
	End Function
End Module