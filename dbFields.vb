Option Strict Off
Option Explicit On
Module dbFields
	
	Public Structure FLDDEF
		Dim fldType As String 'C=char, V=varchar, I1=tinyint, I2=smallint
		'I=int, I8=bigint, D=Date
		Dim fldName As String
		Dim fldOffset As Short
		Dim fldLen As Short
	End Structure
	
	Public arFldDef() As FLDDEF
	Public arOutFldDef() As FLDDEF
	Public iNumFlds As Short
	Public OutNumFlds As Short
	Public Function loadFieldDef(ByRef fileName As String, ByRef out() As FLDDEF) As Short
		Dim iRet, iTmp As Short
		Dim sTmp, sTmp1 As String
		Dim tokens() As String
		Dim fh As Short
		Dim lTmp As Integer
		Dim cnt As Short
		On Error GoTo loadFieldDef_Error
		
		fh = FreeFile
		FileOpen(fh, fileName, OpenMode.Input)
		sTmp = LineInput(fh)
		cnt = Val(sTmp)
		iRet = 0
		ReDim out(cnt)
		Do While iRet < cnt And Not EOF(fh)
			sTmp = LineInput(fh)
			
			iTmp = splitToken(sTmp, tokens, ",")
			If iTmp = 0 Then Exit Do
			
			out(iRet).fldType = tokens(0)
			out(iRet).fldName = tokens(1)
			out(iRet).fldOffset = CShort(tokens(2)) 'start from 1
			out(iRet).fldLen = CShort(tokens(3))
			iRet = iRet + 1
		Loop 
		
		FileClose(fh)
		loadFieldDef = cnt
		Exit Function
		
loadFieldDef_Error: 
		LogMsg("Error loading field definition: " & Err.Description)
		loadFieldDef = 0
	End Function
	
	Public Function splitToken(ByRef sString As String, ByRef aTerms() As String, ByRef strDelim As String) As Short
		Dim sTemp As String
		Dim sNewTerm As String
		Dim iLen As Short
		Dim iCurrentPos As Short
		Dim iLastPos As Short
		Dim iRet As Short
		
		sTemp = Trim(sString)
		iLen = Len(sTemp)
		iRet = 0
		
		If sTemp = "" Then
			splitToken = 0
			GoTo lblExit
		End If
		
		ReDim aTerms(0)
		
		If Left(sTemp, 1) = strDelim Then
			iCurrentPos = InStr(2, sTemp, strDelim)
			If iCurrentPos = iLen Then
				iLastPos = iLen
			Else
				iLastPos = 2
			End If
		Else
			iCurrentPos = InStr(1, sTemp, strDelim)
			iLastPos = 1
		End If
		
		If iCurrentPos <= iLastPos Then
			aTerms(0) = sTemp
			iRet = 1
			GoTo lblExit
		End If
		
		Do Until iCurrentPos > iLen
			Do Until Mid(sTemp, iCurrentPos, 1) = strDelim
				iCurrentPos = iCurrentPos + 1
				If iCurrentPos > iLen Then Exit Do
			Loop 
			
			sNewTerm = Mid(sTemp, iLastPos, iCurrentPos - iLastPos)
			If Not (UBound(aTerms) = 0 And aTerms(UBound(aTerms)) = "") Then
				ReDim Preserve aTerms(UBound(aTerms) + 1)
			End If
			
			aTerms(UBound(aTerms)) = sNewTerm
			iLastPos = iCurrentPos + 1
			iRet = iRet + 1
			
			iCurrentPos = iLastPos
		Loop 
		
lblExit: 
		splitToken = iRet
		Exit Function
		
lblErr: 
		LogMsg("Error in splitToken(): " & Err.Description)
		splitToken = 0
	End Function
End Module