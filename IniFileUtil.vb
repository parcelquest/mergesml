Option Strict Off
Option Explicit On
Module IniFileUtil
	
	'UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1016"'
	'UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1016"'
	Public Declare Function WritePrivateProfileString Lib "kernel32"  Alias "WritePrivateProfileStringA"(ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lpFileName As String) As Integer
	'UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1016"'
	Public Declare Function GetPrivateProfileString Lib "kernel32"  Alias "GetPrivateProfileStringA"(ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer
	'UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1016"'
	Public Declare Function GetPrivateProfileInt Lib "kernel32"  Alias "GetPrivateProfileIntA"(ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lDefault As Short, ByVal lpFileName As String) As Short
	Declare Function GetWindowsDirectory Lib "kernel32"  Alias "GetWindowsDirectoryA"(ByVal lpBuffer As String, ByVal nSize As Integer) As Integer
	
	Function GetIniData(ByRef lpApp As String, ByRef lpSectionName As String, ByRef lpKeyName As String) As String
		Dim lpDefault As String
		Dim lpReturnString As String
		Dim size As Short
		Dim lpFileName As String
		Dim Length As Short
		
		lpDefault = ""
		lpReturnString = Space(256)
		size = Len(lpReturnString)
		lpFileName = VB6.GetPath & "\" & lpApp & ".ini"
		Length = GetPrivateProfileString(lpSectionName, lpKeyName, lpDefault, lpReturnString, size, lpFileName)
		GetIniData = Left(lpReturnString, Length)
	End Function
	
	Function GetIniDataInt(ByRef lpApp As String, ByRef lpSectionName As String, ByRef lpKeyName As String) As Short
		Dim lpFileName As String
		
		lpFileName = VB6.GetPath & "\" & lpApp & ".ini"
		GetIniDataInt = GetPrivateProfileInt(lpSectionName, lpKeyName, 0, lpFileName)
	End Function
	
	Function SaveIniData(ByRef lpApp As String, ByRef lpSectionName As String, ByRef lpKeyName As String, ByRef lpValue As String) As Boolean
		Dim lpDefault As String
		Dim lpReturnString As String
		Dim size As Short
		Dim lpFileName As String
		Dim Length As Short
		
		lpFileName = VB6.GetPath & "\" & lpApp & ".ini"
		SaveIniData = WritePrivateProfileString(lpSectionName, lpKeyName, lpValue, lpFileName)
	End Function
End Module