Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Friend Class frmLoadDB
	Inherits System.Windows.Forms.Form
#Region "Windows Form Designer generated code "
	Public Sub New()
		MyBase.New()
		If m_vb6FormDefInstance Is Nothing Then
			If m_InitializingDefInstance Then
				m_vb6FormDefInstance = Me
			Else
				Try 
					'For the start-up form, the first instance created is the default instance.
					If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
						m_vb6FormDefInstance = Me
					End If
				Catch
				End Try
			End If
		End If
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents chkBig As System.Windows.Forms.CheckBox
	Public WithEvents txtOutputFile As System.Windows.Forms.TextBox
	Public WithEvents cboCounty As System.Windows.Forms.ComboBox
	Public WithEvents cbReset As System.Windows.Forms.CheckBox
	Public WithEvents cmdTruncate As System.Windows.Forms.Button
	Public WithEvents cmdLoadSfxTbl As System.Windows.Forms.Button
	Public WithEvents cmdLoadCityTbl As System.Windows.Forms.Button
	Public WithEvents txtTable As System.Windows.Forms.TextBox
	Public WithEvents txtCounty As System.Windows.Forms.TextBox
	Public WithEvents optCsv As System.Windows.Forms.RadioButton
	Public WithEvents optFix As System.Windows.Forms.RadioButton
	Public WithEvents txtCfgFile As System.Windows.Forms.TextBox
	Public WithEvents txtRecSize As System.Windows.Forms.TextBox
	Public WithEvents txtDatabase As System.Windows.Forms.TextBox
	Public WithEvents txtServer As System.Windows.Forms.TextBox
	Public WithEvents cmdExit As System.Windows.Forms.Button
	Public WithEvents cmdLoadData As System.Windows.Forms.Button
	Public WithEvents txtInputFile As System.Windows.Forms.TextBox
	Public WithEvents _Label6_1 As System.Windows.Forms.Label
	Public WithEvents Label10 As System.Windows.Forms.Label
	Public WithEvents Label9 As System.Windows.Forms.Label
	Public WithEvents Label8 As System.Windows.Forms.Label
	Public WithEvents Label7 As System.Windows.Forms.Label
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents _Label6_0 As System.Windows.Forms.Label
	Public WithEvents Label5 As System.Windows.Forms.Label
	Public WithEvents lblCounter As System.Windows.Forms.Label
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents Label6 As Microsoft.VisualBasic.Compatibility.VB6.LabelArray
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmLoadDB))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.ToolTip1.Active = True
		Me.chkBig = New System.Windows.Forms.CheckBox
		Me.txtOutputFile = New System.Windows.Forms.TextBox
		Me.cboCounty = New System.Windows.Forms.ComboBox
		Me.cbReset = New System.Windows.Forms.CheckBox
		Me.cmdTruncate = New System.Windows.Forms.Button
		Me.cmdLoadSfxTbl = New System.Windows.Forms.Button
		Me.cmdLoadCityTbl = New System.Windows.Forms.Button
		Me.txtTable = New System.Windows.Forms.TextBox
		Me.txtCounty = New System.Windows.Forms.TextBox
		Me.optCsv = New System.Windows.Forms.RadioButton
		Me.optFix = New System.Windows.Forms.RadioButton
		Me.txtCfgFile = New System.Windows.Forms.TextBox
		Me.txtRecSize = New System.Windows.Forms.TextBox
		Me.txtDatabase = New System.Windows.Forms.TextBox
		Me.txtServer = New System.Windows.Forms.TextBox
		Me.cmdExit = New System.Windows.Forms.Button
		Me.cmdLoadData = New System.Windows.Forms.Button
		Me.txtInputFile = New System.Windows.Forms.TextBox
		Me._Label6_1 = New System.Windows.Forms.Label
		Me.Label10 = New System.Windows.Forms.Label
		Me.Label9 = New System.Windows.Forms.Label
		Me.Label8 = New System.Windows.Forms.Label
		Me.Label7 = New System.Windows.Forms.Label
		Me.Label4 = New System.Windows.Forms.Label
		Me._Label6_0 = New System.Windows.Forms.Label
		Me.Label5 = New System.Windows.Forms.Label
		Me.lblCounter = New System.Windows.Forms.Label
		Me.Label3 = New System.Windows.Forms.Label
		Me.Label2 = New System.Windows.Forms.Label
		Me.Label1 = New System.Windows.Forms.Label
		Me.Label6 = New Microsoft.VisualBasic.Compatibility.VB6.LabelArray(components)
		CType(Me.Label6, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Loading data"
		Me.ClientSize = New System.Drawing.Size(525, 158)
		Me.Location = New System.Drawing.Point(3, 22)
		Me.Icon = CType(resources.GetObject("frmLoadDB.Icon"), System.Drawing.Icon)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.KeyPreview = False
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmLoadDB"
		Me.chkBig.Text = "Use &Big"
		Me.chkBig.Size = New System.Drawing.Size(81, 17)
		Me.chkBig.Location = New System.Drawing.Point(168, 80)
		Me.chkBig.TabIndex = 29
		Me.chkBig.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.chkBig.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.chkBig.BackColor = System.Drawing.SystemColors.Control
		Me.chkBig.CausesValidation = True
		Me.chkBig.Enabled = True
		Me.chkBig.ForeColor = System.Drawing.SystemColors.ControlText
		Me.chkBig.Cursor = System.Windows.Forms.Cursors.Default
		Me.chkBig.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.chkBig.Appearance = System.Windows.Forms.Appearance.Normal
		Me.chkBig.TabStop = True
		Me.chkBig.CheckState = System.Windows.Forms.CheckState.Unchecked
		Me.chkBig.Visible = True
		Me.chkBig.Name = "chkBig"
		Me.txtOutputFile.AutoSize = False
		Me.txtOutputFile.ForeColor = System.Drawing.Color.Blue
		Me.txtOutputFile.Size = New System.Drawing.Size(425, 21)
		Me.txtOutputFile.Location = New System.Drawing.Point(76, 54)
		Me.txtOutputFile.TabIndex = 27
		Me.txtOutputFile.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtOutputFile.AcceptsReturn = True
		Me.txtOutputFile.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtOutputFile.BackColor = System.Drawing.SystemColors.Window
		Me.txtOutputFile.CausesValidation = True
		Me.txtOutputFile.Enabled = True
		Me.txtOutputFile.HideSelection = True
		Me.txtOutputFile.ReadOnly = False
		Me.txtOutputFile.Maxlength = 0
		Me.txtOutputFile.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtOutputFile.MultiLine = False
		Me.txtOutputFile.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtOutputFile.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtOutputFile.TabStop = True
		Me.txtOutputFile.Visible = True
		Me.txtOutputFile.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtOutputFile.Name = "txtOutputFile"
		Me.cboCounty.Size = New System.Drawing.Size(229, 21)
		Me.cboCounty.Location = New System.Drawing.Point(168, 168)
		Me.cboCounty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.cboCounty.TabIndex = 26
		Me.cboCounty.Visible = False
		Me.cboCounty.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cboCounty.BackColor = System.Drawing.SystemColors.Window
		Me.cboCounty.CausesValidation = True
		Me.cboCounty.Enabled = True
		Me.cboCounty.ForeColor = System.Drawing.SystemColors.WindowText
		Me.cboCounty.IntegralHeight = True
		Me.cboCounty.Cursor = System.Windows.Forms.Cursors.Default
		Me.cboCounty.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cboCounty.Sorted = False
		Me.cboCounty.TabStop = True
		Me.cboCounty.Name = "cboCounty"
		Me.cbReset.Text = "Don't reset field value when county change"
		Me.cbReset.Size = New System.Drawing.Size(377, 17)
		Me.cbReset.Location = New System.Drawing.Point(18, 188)
		Me.cbReset.TabIndex = 22
		Me.cbReset.Visible = False
		Me.cbReset.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cbReset.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.cbReset.BackColor = System.Drawing.SystemColors.Control
		Me.cbReset.CausesValidation = True
		Me.cbReset.Enabled = True
		Me.cbReset.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cbReset.Cursor = System.Windows.Forms.Cursors.Default
		Me.cbReset.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cbReset.Appearance = System.Windows.Forms.Appearance.Normal
		Me.cbReset.TabStop = True
		Me.cbReset.CheckState = System.Windows.Forms.CheckState.Unchecked
		Me.cbReset.Name = "cbReset"
		Me.cmdTruncate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdTruncate.Text = "&Truncate Data"
		Me.cmdTruncate.Size = New System.Drawing.Size(89, 25)
		Me.cmdTruncate.Location = New System.Drawing.Point(304, 332)
		Me.cmdTruncate.TabIndex = 21
		Me.cmdTruncate.Visible = False
		Me.cmdTruncate.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdTruncate.BackColor = System.Drawing.SystemColors.Control
		Me.cmdTruncate.CausesValidation = True
		Me.cmdTruncate.Enabled = True
		Me.cmdTruncate.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdTruncate.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdTruncate.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdTruncate.TabStop = True
		Me.cmdTruncate.Name = "cmdTruncate"
		Me.cmdLoadSfxTbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdLoadSfxTbl.Text = "&Load SfxTbl"
		Me.cmdLoadSfxTbl.Size = New System.Drawing.Size(89, 25)
		Me.cmdLoadSfxTbl.Location = New System.Drawing.Point(304, 304)
		Me.cmdLoadSfxTbl.TabIndex = 20
		Me.cmdLoadSfxTbl.Visible = False
		Me.cmdLoadSfxTbl.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdLoadSfxTbl.BackColor = System.Drawing.SystemColors.Control
		Me.cmdLoadSfxTbl.CausesValidation = True
		Me.cmdLoadSfxTbl.Enabled = True
		Me.cmdLoadSfxTbl.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdLoadSfxTbl.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdLoadSfxTbl.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdLoadSfxTbl.TabStop = True
		Me.cmdLoadSfxTbl.Name = "cmdLoadSfxTbl"
		Me.cmdLoadCityTbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdLoadCityTbl.Text = "Load &CityTbl"
		Me.cmdLoadCityTbl.Size = New System.Drawing.Size(89, 25)
		Me.cmdLoadCityTbl.Location = New System.Drawing.Point(304, 256)
		Me.cmdLoadCityTbl.TabIndex = 19
		Me.cmdLoadCityTbl.Visible = False
		Me.cmdLoadCityTbl.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdLoadCityTbl.BackColor = System.Drawing.SystemColors.Control
		Me.cmdLoadCityTbl.CausesValidation = True
		Me.cmdLoadCityTbl.Enabled = True
		Me.cmdLoadCityTbl.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdLoadCityTbl.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdLoadCityTbl.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdLoadCityTbl.TabStop = True
		Me.cmdLoadCityTbl.Name = "cmdLoadCityTbl"
		Me.txtTable.AutoSize = False
		Me.txtTable.Size = New System.Drawing.Size(193, 20)
		Me.txtTable.Location = New System.Drawing.Point(80, 304)
		Me.txtTable.TabIndex = 17
		Me.txtTable.Text = "RollTbl"
		Me.txtTable.Visible = False
		Me.txtTable.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtTable.AcceptsReturn = True
		Me.txtTable.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtTable.BackColor = System.Drawing.SystemColors.Window
		Me.txtTable.CausesValidation = True
		Me.txtTable.Enabled = True
		Me.txtTable.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtTable.HideSelection = True
		Me.txtTable.ReadOnly = False
		Me.txtTable.Maxlength = 0
		Me.txtTable.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtTable.MultiLine = False
		Me.txtTable.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtTable.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtTable.TabStop = True
		Me.txtTable.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtTable.Name = "txtTable"
		Me.txtCounty.AutoSize = False
		Me.txtCounty.Size = New System.Drawing.Size(57, 20)
		Me.txtCounty.Location = New System.Drawing.Point(460, 194)
		Me.txtCounty.Maxlength = 3
		Me.txtCounty.TabIndex = 15
		Me.txtCounty.Text = "BUT"
		Me.txtCounty.Visible = False
		Me.txtCounty.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtCounty.AcceptsReturn = True
		Me.txtCounty.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCounty.BackColor = System.Drawing.SystemColors.Window
		Me.txtCounty.CausesValidation = True
		Me.txtCounty.Enabled = True
		Me.txtCounty.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCounty.HideSelection = True
		Me.txtCounty.ReadOnly = False
		Me.txtCounty.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCounty.MultiLine = False
		Me.txtCounty.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCounty.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCounty.TabStop = True
		Me.txtCounty.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtCounty.Name = "txtCounty"
		Me.optCsv.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.optCsv.Text = "CSV"
		Me.optCsv.Size = New System.Drawing.Size(81, 17)
		Me.optCsv.Location = New System.Drawing.Point(424, 256)
		Me.optCsv.TabIndex = 14
		Me.optCsv.Visible = False
		Me.optCsv.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.optCsv.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.optCsv.BackColor = System.Drawing.SystemColors.Control
		Me.optCsv.CausesValidation = True
		Me.optCsv.Enabled = True
		Me.optCsv.ForeColor = System.Drawing.SystemColors.ControlText
		Me.optCsv.Cursor = System.Windows.Forms.Cursors.Default
		Me.optCsv.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.optCsv.Appearance = System.Windows.Forms.Appearance.Normal
		Me.optCsv.TabStop = True
		Me.optCsv.Checked = False
		Me.optCsv.Name = "optCsv"
		Me.optFix.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.optFix.Text = "Fixed Length"
		Me.optFix.Size = New System.Drawing.Size(89, 17)
		Me.optFix.Location = New System.Drawing.Point(424, 288)
		Me.optFix.TabIndex = 13
		Me.optFix.Checked = True
		Me.optFix.Visible = False
		Me.optFix.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.optFix.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.optFix.BackColor = System.Drawing.SystemColors.Control
		Me.optFix.CausesValidation = True
		Me.optFix.Enabled = True
		Me.optFix.ForeColor = System.Drawing.SystemColors.ControlText
		Me.optFix.Cursor = System.Windows.Forms.Cursors.Default
		Me.optFix.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.optFix.Appearance = System.Windows.Forms.Appearance.Normal
		Me.optFix.TabStop = True
		Me.optFix.Name = "optFix"
		Me.txtCfgFile.AutoSize = False
		Me.txtCfgFile.ForeColor = System.Drawing.Color.Blue
		Me.txtCfgFile.Size = New System.Drawing.Size(425, 21)
		Me.txtCfgFile.Location = New System.Drawing.Point(76, 30)
		Me.txtCfgFile.TabIndex = 11
		Me.txtCfgFile.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtCfgFile.AcceptsReturn = True
		Me.txtCfgFile.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCfgFile.BackColor = System.Drawing.SystemColors.Window
		Me.txtCfgFile.CausesValidation = True
		Me.txtCfgFile.Enabled = True
		Me.txtCfgFile.HideSelection = True
		Me.txtCfgFile.ReadOnly = False
		Me.txtCfgFile.Maxlength = 0
		Me.txtCfgFile.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCfgFile.MultiLine = False
		Me.txtCfgFile.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCfgFile.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCfgFile.TabStop = True
		Me.txtCfgFile.Visible = True
		Me.txtCfgFile.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtCfgFile.Name = "txtCfgFile"
		Me.txtRecSize.AutoSize = False
		Me.txtRecSize.ForeColor = System.Drawing.Color.Blue
		Me.txtRecSize.Size = New System.Drawing.Size(65, 20)
		Me.txtRecSize.Location = New System.Drawing.Point(76, 78)
		Me.txtRecSize.TabIndex = 9
		Me.txtRecSize.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtRecSize.AcceptsReturn = True
		Me.txtRecSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtRecSize.BackColor = System.Drawing.SystemColors.Window
		Me.txtRecSize.CausesValidation = True
		Me.txtRecSize.Enabled = True
		Me.txtRecSize.HideSelection = True
		Me.txtRecSize.ReadOnly = False
		Me.txtRecSize.Maxlength = 0
		Me.txtRecSize.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtRecSize.MultiLine = False
		Me.txtRecSize.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtRecSize.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtRecSize.TabStop = True
		Me.txtRecSize.Visible = True
		Me.txtRecSize.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtRecSize.Name = "txtRecSize"
		Me.txtDatabase.AutoSize = False
		Me.txtDatabase.Size = New System.Drawing.Size(193, 20)
		Me.txtDatabase.Location = New System.Drawing.Point(80, 264)
		Me.txtDatabase.TabIndex = 7
		Me.txtDatabase.Text = "BUT"
		Me.txtDatabase.Visible = False
		Me.txtDatabase.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtDatabase.AcceptsReturn = True
		Me.txtDatabase.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtDatabase.BackColor = System.Drawing.SystemColors.Window
		Me.txtDatabase.CausesValidation = True
		Me.txtDatabase.Enabled = True
		Me.txtDatabase.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtDatabase.HideSelection = True
		Me.txtDatabase.ReadOnly = False
		Me.txtDatabase.Maxlength = 0
		Me.txtDatabase.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtDatabase.MultiLine = False
		Me.txtDatabase.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtDatabase.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtDatabase.TabStop = True
		Me.txtDatabase.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtDatabase.Name = "txtDatabase"
		Me.txtServer.AutoSize = False
		Me.txtServer.Size = New System.Drawing.Size(193, 20)
		Me.txtServer.Location = New System.Drawing.Point(80, 224)
		Me.txtServer.TabIndex = 5
		Me.txtServer.Text = "INFOSVR"
		Me.txtServer.Visible = False
		Me.txtServer.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtServer.AcceptsReturn = True
		Me.txtServer.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtServer.BackColor = System.Drawing.SystemColors.Window
		Me.txtServer.CausesValidation = True
		Me.txtServer.Enabled = True
		Me.txtServer.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtServer.HideSelection = True
		Me.txtServer.ReadOnly = False
		Me.txtServer.Maxlength = 0
		Me.txtServer.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtServer.MultiLine = False
		Me.txtServer.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtServer.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtServer.TabStop = True
		Me.txtServer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtServer.Name = "txtServer"
		Me.cmdExit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdExit.Text = "E&xit"
		Me.cmdExit.Size = New System.Drawing.Size(89, 25)
		Me.cmdExit.Location = New System.Drawing.Point(412, 82)
		Me.cmdExit.TabIndex = 3
		Me.cmdExit.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdExit.BackColor = System.Drawing.SystemColors.Control
		Me.cmdExit.CausesValidation = True
		Me.cmdExit.Enabled = True
		Me.cmdExit.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdExit.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdExit.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdExit.TabStop = True
		Me.cmdExit.Name = "cmdExit"
		Me.cmdLoadData.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdLoadData.Text = "Load &Data"
		Me.cmdLoadData.Size = New System.Drawing.Size(89, 25)
		Me.cmdLoadData.Location = New System.Drawing.Point(320, 82)
		Me.cmdLoadData.TabIndex = 2
		Me.cmdLoadData.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdLoadData.BackColor = System.Drawing.SystemColors.Control
		Me.cmdLoadData.CausesValidation = True
		Me.cmdLoadData.Enabled = True
		Me.cmdLoadData.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdLoadData.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdLoadData.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdLoadData.TabStop = True
		Me.cmdLoadData.Name = "cmdLoadData"
		Me.txtInputFile.AutoSize = False
		Me.txtInputFile.ForeColor = System.Drawing.Color.Blue
		Me.txtInputFile.Size = New System.Drawing.Size(425, 21)
		Me.txtInputFile.Location = New System.Drawing.Point(76, 6)
		Me.txtInputFile.TabIndex = 1
		Me.txtInputFile.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtInputFile.AcceptsReturn = True
		Me.txtInputFile.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtInputFile.BackColor = System.Drawing.SystemColors.Window
		Me.txtInputFile.CausesValidation = True
		Me.txtInputFile.Enabled = True
		Me.txtInputFile.HideSelection = True
		Me.txtInputFile.ReadOnly = False
		Me.txtInputFile.Maxlength = 0
		Me.txtInputFile.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtInputFile.MultiLine = False
		Me.txtInputFile.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtInputFile.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtInputFile.TabStop = True
		Me.txtInputFile.Visible = True
		Me.txtInputFile.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtInputFile.Name = "txtInputFile"
		Me._Label6_1.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me._Label6_1.Text = "Output File"
		Me._Label6_1.Size = New System.Drawing.Size(57, 17)
		Me._Label6_1.Location = New System.Drawing.Point(10, 54)
		Me._Label6_1.TabIndex = 28
		Me._Label6_1.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._Label6_1.BackColor = System.Drawing.SystemColors.Control
		Me._Label6_1.Enabled = True
		Me._Label6_1.ForeColor = System.Drawing.SystemColors.ControlText
		Me._Label6_1.Cursor = System.Windows.Forms.Cursors.Default
		Me._Label6_1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label6_1.UseMnemonic = True
		Me._Label6_1.Visible = True
		Me._Label6_1.AutoSize = False
		Me._Label6_1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label6_1.Name = "_Label6_1"
		Me.Label10.ForeColor = System.Drawing.Color.Red
		Me.Label10.Size = New System.Drawing.Size(63, 21)
		Me.Label10.Location = New System.Drawing.Point(342, 116)
		Me.Label10.TabIndex = 25
		Me.Label10.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label10.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label10.BackColor = System.Drawing.SystemColors.Control
		Me.Label10.Enabled = True
		Me.Label10.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label10.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label10.UseMnemonic = True
		Me.Label10.Visible = True
		Me.Label10.AutoSize = False
		Me.Label10.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label10.Name = "Label10"
		Me.Label9.Size = New System.Drawing.Size(77, 21)
		Me.Label9.Location = New System.Drawing.Point(260, 116)
		Me.Label9.TabIndex = 24
		Me.Label9.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label9.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label9.BackColor = System.Drawing.SystemColors.Control
		Me.Label9.Enabled = True
		Me.Label9.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label9.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label9.UseMnemonic = True
		Me.Label9.Visible = True
		Me.Label9.AutoSize = False
		Me.Label9.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label9.Name = "Label9"
		Me.Label8.Size = New System.Drawing.Size(77, 21)
		Me.Label8.Location = New System.Drawing.Point(178, 116)
		Me.Label8.TabIndex = 23
		Me.Label8.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label8.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label8.BackColor = System.Drawing.SystemColors.Control
		Me.Label8.Enabled = True
		Me.Label8.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label8.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label8.UseMnemonic = True
		Me.Label8.Visible = True
		Me.Label8.AutoSize = False
		Me.Label8.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label8.Name = "Label8"
		Me.Label7.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.Label7.Text = "Table name"
		Me.Label7.Size = New System.Drawing.Size(73, 17)
		Me.Label7.Location = New System.Drawing.Point(0, 304)
		Me.Label7.TabIndex = 18
		Me.Label7.Visible = False
		Me.Label7.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label7.BackColor = System.Drawing.SystemColors.Control
		Me.Label7.Enabled = True
		Me.Label7.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label7.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label7.UseMnemonic = True
		Me.Label7.AutoSize = False
		Me.Label7.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label7.Name = "Label7"
		Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.Label4.Text = "County"
		Me.Label4.Size = New System.Drawing.Size(73, 17)
		Me.Label4.Location = New System.Drawing.Point(80, 168)
		Me.Label4.TabIndex = 16
		Me.Label4.Visible = False
		Me.Label4.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label4.BackColor = System.Drawing.SystemColors.Control
		Me.Label4.Enabled = True
		Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label4.UseMnemonic = True
		Me.Label4.AutoSize = False
		Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label4.Name = "Label4"
		Me._Label6_0.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me._Label6_0.Text = "Field Def."
		Me._Label6_0.Size = New System.Drawing.Size(57, 17)
		Me._Label6_0.Location = New System.Drawing.Point(10, 30)
		Me._Label6_0.TabIndex = 12
		Me._Label6_0.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._Label6_0.BackColor = System.Drawing.SystemColors.Control
		Me._Label6_0.Enabled = True
		Me._Label6_0.ForeColor = System.Drawing.SystemColors.ControlText
		Me._Label6_0.Cursor = System.Windows.Forms.Cursors.Default
		Me._Label6_0.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label6_0.UseMnemonic = True
		Me._Label6_0.Visible = True
		Me._Label6_0.AutoSize = False
		Me._Label6_0.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label6_0.Name = "_Label6_0"
		Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.Label5.Text = "RecSize"
		Me.Label5.Size = New System.Drawing.Size(57, 17)
		Me.Label5.Location = New System.Drawing.Point(12, 80)
		Me.Label5.TabIndex = 10
		Me.Label5.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label5.BackColor = System.Drawing.SystemColors.Control
		Me.Label5.Enabled = True
		Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label5.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label5.UseMnemonic = True
		Me.Label5.Visible = True
		Me.Label5.AutoSize = False
		Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label5.Name = "Label5"
		Me.lblCounter.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lblCounter.ForeColor = System.Drawing.Color.Red
		Me.lblCounter.Size = New System.Drawing.Size(165, 33)
		Me.lblCounter.Location = New System.Drawing.Point(10, 106)
		Me.lblCounter.TabIndex = 8
		Me.lblCounter.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblCounter.BackColor = System.Drawing.SystemColors.Control
		Me.lblCounter.Enabled = True
		Me.lblCounter.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblCounter.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblCounter.UseMnemonic = True
		Me.lblCounter.Visible = True
		Me.lblCounter.AutoSize = False
		Me.lblCounter.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblCounter.Name = "lblCounter"
		Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.Label3.Text = "DB name"
		Me.Label3.Size = New System.Drawing.Size(73, 17)
		Me.Label3.Location = New System.Drawing.Point(0, 264)
		Me.Label3.TabIndex = 6
		Me.Label3.Visible = False
		Me.Label3.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label3.BackColor = System.Drawing.SystemColors.Control
		Me.Label3.Enabled = True
		Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label3.UseMnemonic = True
		Me.Label3.AutoSize = False
		Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label3.Name = "Label3"
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.Label2.Text = "Server name"
		Me.Label2.Size = New System.Drawing.Size(73, 17)
		Me.Label2.Location = New System.Drawing.Point(0, 224)
		Me.Label2.TabIndex = 4
		Me.Label2.Visible = False
		Me.Label2.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label2.BackColor = System.Drawing.SystemColors.Control
		Me.Label2.Enabled = True
		Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.UseMnemonic = True
		Me.Label2.AutoSize = False
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.Name = "Label2"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.Label1.Text = "File name"
		Me.Label1.Size = New System.Drawing.Size(57, 17)
		Me.Label1.Location = New System.Drawing.Point(10, 8)
		Me.Label1.TabIndex = 0
		Me.Label1.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.Controls.Add(chkBig)
		Me.Controls.Add(txtOutputFile)
		Me.Controls.Add(cboCounty)
		Me.Controls.Add(cbReset)
		Me.Controls.Add(cmdTruncate)
		Me.Controls.Add(cmdLoadSfxTbl)
		Me.Controls.Add(cmdLoadCityTbl)
		Me.Controls.Add(txtTable)
		Me.Controls.Add(txtCounty)
		Me.Controls.Add(optCsv)
		Me.Controls.Add(optFix)
		Me.Controls.Add(txtCfgFile)
		Me.Controls.Add(txtRecSize)
		Me.Controls.Add(txtDatabase)
		Me.Controls.Add(txtServer)
		Me.Controls.Add(cmdExit)
		Me.Controls.Add(cmdLoadData)
		Me.Controls.Add(txtInputFile)
		Me.Controls.Add(_Label6_1)
		Me.Controls.Add(Label10)
		Me.Controls.Add(Label9)
		Me.Controls.Add(Label8)
		Me.Controls.Add(Label7)
		Me.Controls.Add(Label4)
		Me.Controls.Add(_Label6_0)
		Me.Controls.Add(Label5)
		Me.Controls.Add(lblCounter)
		Me.Controls.Add(Label3)
		Me.Controls.Add(Label2)
		Me.Controls.Add(Label1)
		Me.Label6.SetIndex(_Label6_1, CType(1, Short))
		Me.Label6.SetIndex(_Label6_0, CType(0, Short))
		CType(Me.Label6, System.ComponentModel.ISupportInitialize).EndInit()
	End Sub
#End Region 
#Region "Upgrade Support "
	Private Shared m_vb6FormDefInstance As frmLoadDB
	Private Shared m_InitializingDefInstance As Boolean
	Public Shared Property DefInstance() As frmLoadDB
		Get
			If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
				m_InitializingDefInstance = True
				m_vb6FormDefInstance = New frmLoadDB()
				m_InitializingDefInstance = False
			End If
			DefInstance = m_vb6FormDefInstance
		End Get
		Set
			m_vb6FormDefInstance = Value
		End Set
	End Property
#End Region 
	Dim OutputDisk As Short
	Dim counter As Integer
	
	Private Function OpenOutputFile(ByRef f As String) As Short
		LogMsg("Open output file #" & OutputDisk)
		
		OutFile = FreeFile
		FileOpen(OutFile, f & ".R" & VB6.Format(OutputDisk, "00"), OpenMode.Random, , , Len(OutRecSML))
		OutputDisk = OutputDisk + 1
	End Function
	
	Private Function CloseOutputFile() As Short
		FileClose(OutFile)
	End Function
	
	Private Function OpenBigFile(ByRef fname As String) As Short
		LogMsg("Open output file " & fname)
		fcOut = New clsBigFile
		fcOut.OpenFile(fname)
	End Function
	
	Private Function CloseBigFile() As Short
		fcOut.CloseFile()
	End Function
	
	Private Function LoadCounty() As Boolean
		cboCounty.Items.Add("ALA Alameda")
		cboCounty.Items.Add("ALP Alpine")
		cboCounty.Items.Add("AMA Amador")
		cboCounty.Items.Add("BUT Butte")
		cboCounty.Items.Add("CAL Calaveras")
		cboCounty.Items.Add("CCX Contra Costa")
		cboCounty.Items.Add("COL Colusa")
		cboCounty.Items.Add("DNX Del Norte")
		cboCounty.Items.Add("EDX El Dorado")
		cboCounty.Items.Add("FRE Fresno")
		cboCounty.Items.Add("GLE Glenn")
		cboCounty.Items.Add("HUM Humboldt")
		cboCounty.Items.Add("IMP Imperial")
		cboCounty.Items.Add("INY Inyo")
		cboCounty.Items.Add("KER Kern")
		cboCounty.Items.Add("KIN King")
		cboCounty.Items.Add("LAK Lake")
		cboCounty.Items.Add("LAS Lassen")
		cboCounty.Items.Add("LAX Los Angeles")
		cboCounty.Items.Add("MAD Madera")
		cboCounty.Items.Add("MEN Mendocino")
		cboCounty.Items.Add("MER Merced")
		cboCounty.Items.Add("MNO Mono")
		cboCounty.Items.Add("MOD Modoc")
		cboCounty.Items.Add("MON Monterey")
		cboCounty.Items.Add("MPA Mariposa")
		cboCounty.Items.Add("MRN Marin")
		cboCounty.Items.Add("NAP Napa")
		cboCounty.Items.Add("NEV Nevada")
		cboCounty.Items.Add("ORG Orange")
		cboCounty.Items.Add("PLA Placer")
		cboCounty.Items.Add("PLU Plumas")
		cboCounty.Items.Add("RIV Riverside")
		cboCounty.Items.Add("SAC Sacramento")
		cboCounty.Items.Add("SBD San Bernadino")
		cboCounty.Items.Add("SBT San Benito")
		cboCounty.Items.Add("SBX Santa Barbara")
		cboCounty.Items.Add("SCL Santa Clara")
		cboCounty.Items.Add("SCR Santa Cruz")
		cboCounty.Items.Add("SDX San Diego")
		cboCounty.Items.Add("SFX San Francisco")
		cboCounty.Items.Add("SHA Shasta")
		cboCounty.Items.Add("SIE Sierra")
		cboCounty.Items.Add("SIS Siskiyou")
		cboCounty.Items.Add("SJX San Joaquin")
		cboCounty.Items.Add("SLO San Louis Obispo")
		cboCounty.Items.Add("SMX San Mateo")
		cboCounty.Items.Add("SOL Solano")
		cboCounty.Items.Add("SON Sonoma")
		cboCounty.Items.Add("STA Stanislaus")
		cboCounty.Items.Add("SUT Sutter")
		cboCounty.Items.Add("TEH Tehama")
		cboCounty.Items.Add("TRI Trinity")
		cboCounty.Items.Add("TUL Tulare")
		cboCounty.Items.Add("TUO Tuolumne")
		cboCounty.Items.Add("VEN Ventura")
		cboCounty.Items.Add("YOL Yolo")
		cboCounty.Items.Add("YUB Yuba")
	End Function
	
	'UPGRADE_WARNING: Event cboCounty.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"'
	Private Sub cboCounty_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cboCounty.SelectedIndexChanged
		txtCounty.Text = Mid(cboCounty.Text, 1, InStr(cboCounty.Text, " ") - 1)
		cmdLoadData.Enabled = True
		txtInputFile.Text = g_sInputFile & "\S" & txtCounty.Text & "_CD\RAW\S" & txtCounty.Text
		g_sOutputFile = txtInputFile.Text
	End Sub
	
	
	'UPGRADE_WARNING: Event chkBig.CheckStateChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"'
	Private Sub chkBig_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkBig.CheckStateChanged
		If chkBig.CheckState = System.Windows.Forms.CheckState.Checked Then
			g_bUseBig = True
		Else
			g_bUseBig = False
		End If
	End Sub
	
	Private Sub cmdExit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdExit.Click
		If Not bLoading Then
			Me.Close()
		Else
			bStop = True
		End If
	End Sub
	
	Private Sub cmdLoadData_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdLoadData.Click
		Dim iRet As Short
		Dim sPath As String
		Dim Finish, Start, TotalTime As Object
		Dim CountyList As Short
		
		frmLoadDB.DefInstance.Label8.Text = CStr(TimeOfDay)
		OutputDisk = 1
		
		'Turn off all buttons
		Call switchButtons(False)
		
		g_iRecSize = Val(txtRecSize.Text)
		
		On Error GoTo cmdLoadData_Error
		
		LogMsg("Loading suffix table ...")
		iRet = loadSuffixTbl(g_sSfxTable)
		If iRet = 0 Then
			LogMsg("Error loading suffix table: " & g_sSfxTable)
			Exit Sub
		End If
		
		
		LogMsg("Loading input field definition ...")
		iNumFlds = loadFieldDef((txtCfgFile.Text), arFldDef)
		If iNumFlds = 0 Then
			MsgBox("Error loading Field definition file for " & txtCfgFile.Text)
			Exit Sub
		End If
		
		'LogMsg "Loading output field definition ..."
		'iRet = loadFieldDef(g_sOutFieldDef, arOutFldDef)
		'If iRet = 0 Then
		'   MsgBox "Error loading Field definition file for " & g_sOutFieldDef
		'   Exit Sub
		'End If
		
		'UPGRADE_WARNING: Couldn't resolve default property of object Start. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
		Start = VB.Timer() ' Set start time.
		lCnt = 0
		If g_bUseBig = False Then
			rc = OpenOutputFile(g_sOutputFile)
		Else
			rc = OpenBigFile(g_sOutputFile)
		End If
		If VB.Right(g_sInputFile, 1) <> "\" Then
			g_sInputFile = g_sInputFile & "\"
		End If
		
		For CountyList = 0 To cboCounty.Items.Count - 1
			txtCounty.Text = Mid(VB6.GetItemString(cboCounty, CountyList), 1, InStr(VB6.GetItemString(cboCounty, CountyList), " ") - 1)
			txtInputFile.Text = g_sInputFile & "S" & txtCounty.Text
			sPath = g_sInputFile
			
			bLoading = True
			bStop = False
			
			'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"'
			InputFile = Dir(txtInputFile.Text & "." & g_sFixExt)
			Do While InputFile <> ""
				iRet = loadCityTbl(txtCounty.Text)
				If iRet = 0 Then
					LogMsg("Error loading city table for: " & txtCounty.Text)
					Exit Sub
				End If
				
				If Not setCounty(txtCounty.Text) Then
					MsgBox("Error initialize county " & txtCounty.Text)
				End If
				
				InputFile = sPath & InputFile
				Me.Text = "Loading " & InputFile
				LogMsg("Loading " & InputFile)
				If g_bUseBig = False Then
					Call loadData1(txtCounty.Text)
				Else
					Call loadG01(txtCounty.Text)
				End If
				System.Windows.Forms.Application.DoEvents()
				If bStop Then
					Exit For
				End If
				'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"'
				InputFile = Dir()
			Loop 
		Next 
		
		bLoading = False
		'UPGRADE_WARNING: Couldn't resolve default property of object Finish. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
		Finish = VB.Timer() ' Set end time.
		'UPGRADE_WARNING: Couldn't resolve default property of object Start. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
		'UPGRADE_WARNING: Couldn't resolve default property of object Finish. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
		'UPGRADE_WARNING: Couldn't resolve default property of object TotalTime. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
		TotalTime = Finish - Start ' Calculate total time.
		'UPGRADE_WARNING: Couldn't resolve default property of object TotalTime. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
		frmLoadDB.DefInstance.Label10.Text = CStr(TotalTime / 60)
		If g_bUseBig Then
			rc = CloseBigFile()
		Else
			rc = CloseOutputFile()
		End If
		
		Call switchButtons(True)
		
		Beep()
		Beep()
		Beep()
		MsgBox("Merging SML Completed", MsgBoxStyle.Information + MsgBoxStyle.OKOnly, "MergeSML")
		Exit Sub
cmdLoadData_Error: 
		MsgBox("Error occured: " & Err.Description)
		Call switchButtons(True)
	End Sub
	
	'Loading R01 file
	Public Sub loadData1(ByVal sCnty As String)
		Dim iCnt, iRet, fh, iTmp As Short
		Dim sql As String
		Dim strError As String
		Dim lRet, lTmp As Integer
		Dim dTmp As Double
		Dim sTemp As Object
		Dim strTmp, sTmp As String
		Dim Fmt As String
		Dim MaxCount As Integer
		
		Dim sApn As String
		Dim SADDR As String
		Dim sCity As String
		Dim MADDR As String
		Dim MCity As String
		
		Dim bError As Boolean
		Dim ErrorCount As Integer
		
		ErrorCount = 0
		iRet = -1
		On Error GoTo loadDB1_Error
		
		LogMsg("Loop through " & InputFile)
		MaxCount = 0
		
		OutRecSML.COID = sCnty
		OutRecSML.ADDRMCH = " "
		OutRecSML.HOMCH = " "
		
		fc = New clsBigFile
		fc.OpenFile(InputFile)
		Do While True
			MaxCount = MaxCount + 1
			
			lRet = fc.ReadRecs(g_iRecSize, strTmp)
			If lRet <> g_iRecSize Then
				LogMsg("End of file @" & lCnt)
				Exit Do
			End If
			bError = False
			If VB.Left(strTmp, 10) <> "9999999999" Then
				SADDR = ""
				sCity = ""
				MADDR = ""
				MCity = ""
				
				iRet = -2
				lCnt = lCnt + 1
				counter = counter + 1
				For iCnt = 0 To iNumFlds - 1
					sTmp = Trim(Mid(strTmp, arFldDef(iCnt).fldOffset, arFldDef(iCnt).fldLen))
					'            Select Case arFldDef(iCnt).fldName
					'               Case "S_FRA", "S_DIR"
					'                  SADDR = SADDR & IIf(Len(Fmt), Fmt & " ", "")
					'               Case "S_ST"
					'                  sCity = sCity & " CA"
					'               Case "M_FRA", "M_DIR"
					'                  MADDR = MADDR & IIf(Len(Fmt), Fmt & " ", "")
					'               Case "M_ST"
					'                  MCity = MCity & IIf(Len(Fmt), Fmt & " ", "")
					'               Case "S_STREET", "S_UNIT"
					'                  SADDR = SADDR & IIf(Len(Fmt), Fmt & " ", "")
					'               Case "S_CITY"
					'                  If Len(Fmt) > 0 And g_iCities > Fmt Then
					'                     sCity = sCity & g_sCities(Val(Fmt))
					'                  Else
					'                     LogMsg "*** Error loading data at record " & counter & ", City not found = " & Fmt
					'                     ErrorCount = ErrorCount + 1
					'                     'bError = True
					'                     'Exit For
					'                     sCity = " "
					'                  End If
					'               Case "S_SUFFIX"
					'                  If Len(Fmt) > 0 And g_iSuffixes > Fmt Then
					'                     SADDR = SADDR & g_sSuffixes(Val(Fmt)) & " "
					'                  End If
					'
					'               Case "M_STREET", "M_UNIT"
					'                  MADDR = MADDR & IIf(Len(Fmt), Fmt & " ", "")
					'               Case "M_CITY"
					'                  If Len(Fmt) > 0 Then
					'                     MCity = MCity & Fmt & " "
					'                  End If
					'               Case "M_SUFFIX"
					'                  If Len(Fmt) > 0 Then
					'                     MADDR = MADDR & Fmt & " "
					'                  End If
					'
					'               Case "STATUS"
					'                  sTmp = Mid$(Trim(Mid(strTmp, arFldDef(iCnt).fldOffset, arFldDef(iCnt).fldLen)), 1, 1)
					'               Case "USE_CO"
					'                  sTmp = Mid$(Trim(Mid(strTmp, arFldDef(iCnt).fldOffset, arFldDef(iCnt).fldLen)), 1, 6)
					'               Case "APN"
					'                  sApn = Fmt
					'                  sTmp = Fmt
					'               Case "FMT_APN"
					'                  If Fmt > "0" Then
					'                     sTmp = Fmt
					'                  Else
					'                     sTmp = reFormatApn(sApn)
					'                  End If
					'               Case "S_ZIP4"
					'                  sCity = sCity & IIf((Fmt = "0" Or Len(Fmt) = 0), "", "-" & Fmt)
					'               Case "M_ZIP4"
					'                  MCity = MCity & IIf((Fmt = "0" Or Len(Fmt) = 0), "", "-" & Fmt)
					'               Case "S_CITY"
					'                  If Len(Fmt) > 0 And g_iCities > Fmt Then
					'                     sCity = sCity & g_sCities(Val(Fmt))
					'                  Else
					'                     LogMsg "*** Error loading data at record " & counter & ", City not found = " & Fmt
					'                     ErrorCount = ErrorCount + 1
					'                     'bError = True
					'                     'Exit For
					'                     sCity = " "
					'                  End If
					'               Case "S_SUFFIX"
					'                  If Len(Fmt) > 0 And g_iSuffixes > Fmt Then
					'                     SADDR = SADDR & g_sSuffixes(Val(Fmt)) & " "
					'                  End If
					'               Case "S_STRNUM"
					'                  SADDR = SADDR & IIf((Fmt = "0" Or Len(Fmt) = 0), "", Fmt & " ")
					'               Case "S_ZIP"
					'                  sCity = sCity & IIf(Fmt = "0" Or Len(Fmt) = 0, "", " " & Fmt)
					'               Case "M_STRNUM"
					'                  MADDR = MADDR & IIf((Fmt = "0" Or Len(Fmt) = 0), "", Fmt & " ")
					'               Case "M_ZIP"
					'                  MCity = MCity & IIf(Fmt = "0" Or Len(Fmt) = 0, "", Fmt)
					'               Case Else
					'                  sTmp = Fmt
					'            End Select
					
					Select Case arFldDef(iCnt).fldName
						Case "APN"
							OutRecSML.APNS = sTmp
						Case "FMT_APN"
							OutRecSML.APND = sTmp
							'Case "CNTYCODE"
							'   OutRecSML.COID = sCnty
						Case "HO_FL"
							If sTmp = "1" Then
								OutRecSML.HOEXE = "Y"
							Else
								OutRecSML.HOEXE = "N"
							End If
						Case "USE_CO"
							OutRecSML.USECODE = sTmp
						Case "STATUS"
							OutRecSML.Status2 = sTmp
						Case "TRA"
							OutRecSML.TRA = sTmp
						Case "NAME1"
							OutRecSML.NAME1 = sTmp
						Case "NAME2"
							OutRecSML.NAME2 = sTmp
						Case "GROSS_VAL"
							OutRecSML.GROSS = sTmp
						Case "S_ADDR_D"
							OutRecSML.SADDR = sTmp
						Case "S_CTY_ST_D"
							OutRecSML.SCTYST = sTmp
						Case "M_ADDR_D"
							OutRecSML.MADDR = sTmp
						Case "M_CTY_ST_D"
							MCity = sTmp
						Case "M_ZIP"
							MCity = MCity & " " & sTmp
					End Select
				Next 
				
				OutRecSML.MCTYST = MCity
				OutRecSML.ADDRMCH = " "
				OutRecSML.HOMCH = " "
				'UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"'
				FilePut(OutFile, OutRecSML, counter)
				System.Windows.Forms.Application.DoEvents()
				If (lCnt Mod g_RecCount) = 0 Then
					counter = 0
					rc = CloseOutputFile()
					rc = OpenOutputFile(g_sOutputFile)
				End If
				If (lCnt Mod 10000) = 0 And g_bAutoLoad = False Then
					frmLoadDB.DefInstance.lblCounter.Text = VB6.Format(lCnt, "#,###,###,###")
					frmLoadDB.DefInstance.Label9.Text = CStr(TimeOfDay)
					If bStop Then
						If MsgBox("Are you sure you want to stop?", MsgBoxStyle.YesNo, "Stop Loading") = MsgBoxResult.Yes Then
							LogMsg("Stopped by user request at " & counter & " on " & g_sInputFile)
							rc = CloseOutputFile()
							Exit Do
						End If
					End If
				End If
			End If
		Loop 
		LogMsg("Complete " & g_sInputFile & " with total " & MaxCount & " records")
		If ErrorCount > 0 Then
			LogMsg("Complete " & g_sInputFile & " with total error " & ErrorCount & " records")
		End If
		
		fc.CloseFile()
		iRet = 0
		If g_bAutoLoad = False Then frmLoadDB.DefInstance.lblCounter.Text = CStr(lCnt)
		
loadDB1_Error: 
		On Error Resume Next
		
		If iRet < 0 Then
			'UPGRADE_WARNING: Couldn't resolve default property of object sTemp. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
			LogMsg("Error loading data at record " & lCnt & ", field " & arFldDef(iCnt).fldName & ", value " & sTemp)
			ErrorCount = ErrorCount + 1
		Else
			LogMsg("Loading data complete on " & InputFile)
		End If
		'UPGRADE_NOTE: Object fc may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1029"'
		fc = Nothing
	End Sub
	
	'Loading R01 file
	Public Sub loadData(ByVal sCnty As String)
		Dim iCnt, iRet, fh, iTmp As Short
		Dim sql As String
		Dim strError As String
		Dim lRet, lTmp As Integer
		Dim dTmp As Double
		Dim sTemp As Object
		Dim strTmp, sTmp As String
		Dim Fmt As String
		Dim MaxCount As Integer
		
		Dim sApn As String
		Dim SADDR As String
		Dim sCity As String
		Dim MADDR As String
		Dim MCity As String
		
		Dim bError As Boolean
		Dim ErrorCount As Integer
		
		ErrorCount = 0
		iRet = -1
		On Error GoTo loadDB_Error
		
		LogMsg("Loop through " & InputFile)
		MaxCount = 0
		
		OutRecSML.COID = sCnty
		OutRecSML.ADDRMCH = " "
		OutRecSML.HOMCH = " "
		
		fc = New clsBigFile
		fc.OpenFile(InputFile)
		Do While True
			MaxCount = MaxCount + 1
			
			lRet = fc.ReadRecs(g_iRecSize, strTmp)
			If lRet <> g_iRecSize Then
				LogMsg("End of file @" & lCnt)
				Exit Do
			End If
			bError = False
			If VB.Left(strTmp, 10) <> "9999999999" Then
				SADDR = ""
				sCity = ""
				MADDR = ""
				MCity = ""
				
				iRet = -2
				lCnt = lCnt + 1
				counter = counter + 1
				For iCnt = 0 To iNumFlds
					Select Case arFldDef(iCnt).fldName
						Case "APN", "FMT_APN", "STATUS", "HO_FL", "STATUS", "USE_CO", "TRA", "NAME1", "NAME2", "GROSS_VAL", "S_STRNUM", "S_FRA", "S_DIR", "S_STREET", "S_SUFFIX", "S_UNIT", "S_CITY", "S_ST", "S_ZIP", "S_ZIP4", "M_STRNUM", "M_FRA", "M_DIR", "M_STREET", "M_SUFFIX", "M_UNIT", "M_CITY", "M_ST", "M_ZIP", "M_ZIP4"
							Select Case arFldDef(iCnt).fldType
								Case "C"
									Fmt = Trim(Mid(strTmp, arFldDef(iCnt).fldOffset, arFldDef(iCnt).fldLen))
									Select Case arFldDef(iCnt).fldName
										Case "S_FRA", "S_DIR"
											SADDR = SADDR & IIf(Len(Fmt), Fmt & " ", "")
										Case "S_ST"
											sCity = sCity & " CA"
										Case "M_FRA", "M_DIR"
											MADDR = MADDR & IIf(Len(Fmt), Fmt & " ", "")
										Case "M_ST"
											MCity = MCity & IIf(Len(Fmt), Fmt & " ", "")
										Case Else
											sTmp = Fmt
									End Select
								Case "V", "NV"
									Fmt = Trim(Mid(strTmp, arFldDef(iCnt).fldOffset, arFldDef(iCnt).fldLen))
									Select Case arFldDef(iCnt).fldName
										Case "S_STREET", "S_UNIT"
											SADDR = SADDR & IIf(Len(Fmt), Fmt & " ", "")
										Case "S_CITY"
											If Len(Fmt) > 0 And g_iCities > CDbl(Fmt) Then
												sCity = sCity & g_sCities(Val(Fmt))
											Else
												LogMsg("*** Error loading data at record " & counter & ", City not found = " & Fmt)
												ErrorCount = ErrorCount + 1
												'bError = True
												'Exit For
												sCity = " "
											End If
										Case "S_SUFFIX"
											If Len(Fmt) > 0 And g_iSuffixes > CDbl(Fmt) Then
												SADDR = SADDR & g_sSuffixes(Val(Fmt)) & " "
											End If
											
										Case "M_STREET", "M_UNIT"
											MADDR = MADDR & IIf(Len(Fmt), Fmt & " ", "")
										Case "M_CITY"
											If Len(Fmt) > 0 Then
												MCity = MCity & Fmt & " "
											End If
										Case "M_SUFFIX"
											If Len(Fmt) > 0 Then
												MADDR = MADDR & Fmt & " "
											End If
											
										Case "STATUS"
											sTmp = Mid(Trim(Mid(strTmp, arFldDef(iCnt).fldOffset, arFldDef(iCnt).fldLen)), 1, 1)
										Case "USE_CO"
											sTmp = Mid(Trim(Mid(strTmp, arFldDef(iCnt).fldOffset, arFldDef(iCnt).fldLen)), 1, 6)
										Case "APN"
											sApn = Fmt
											sTmp = Fmt
										Case "FMT_APN"
											If Fmt > "0" Then
												sTmp = Fmt
											Else
												sTmp = reFormatApn(sApn)
											End If
										Case Else
											sTmp = Fmt
									End Select
								Case "I2"
									Fmt = Trim(VB6.Format(Val(Mid(strTmp, arFldDef(iCnt).fldOffset, arFldDef(iCnt).fldLen))))
									Select Case arFldDef(iCnt).fldName
										Case "S_ZIP4"
											sCity = sCity & IIf((Fmt = "0" Or Len(Fmt) = 0), "", "-" & Fmt)
										Case "M_ZIP4"
											MCity = MCity & IIf((Fmt = "0" Or Len(Fmt) = 0), "", "-" & Fmt)
										Case "S_CITY"
											If Len(Fmt) > 0 And g_iCities > CDbl(Fmt) Then
												sCity = sCity & g_sCities(Val(Fmt))
											Else
												LogMsg("*** Error loading data at record " & counter & ", City not found = " & Fmt)
												ErrorCount = ErrorCount + 1
												'bError = True
												'Exit For
												sCity = " "
											End If
										Case "S_SUFFIX"
											If Len(Fmt) > 0 And g_iSuffixes > CDbl(Fmt) Then
												SADDR = SADDR & g_sSuffixes(Val(Fmt)) & " "
											End If
										Case Else
											lTmp = Val(Mid(strTmp, arFldDef(iCnt).fldOffset, arFldDef(iCnt).fldLen))
											If lTmp > 32767 Then
												LogMsg("*** Invalid value for I2: " & lTmp & " at rec# " & lCnt)
												ErrorCount = ErrorCount + 1
											Else
												'                        dbRs(arFldDef(iCnt).fldName).Value = lTmp
											End If
									End Select
								Case "I4"
									Fmt = Trim(VB6.Format(Val(Mid(strTmp, arFldDef(iCnt).fldOffset, arFldDef(iCnt).fldLen))))
									Select Case arFldDef(iCnt).fldName
										Case "S_STRNUM"
											SADDR = SADDR & IIf((Fmt = "0" Or Len(Fmt) = 0), "", Fmt & " ")
										Case "S_ZIP"
											sCity = sCity & IIf(Fmt = "0" Or Len(Fmt) = 0, "", " " & Fmt)
										Case "M_STRNUM"
											MADDR = MADDR & IIf((Fmt = "0" Or Len(Fmt) = 0), "", Fmt & " ")
										Case "M_ZIP"
											MCity = MCity & IIf(Fmt = "0" Or Len(Fmt) = 0, "", Fmt)
										Case Else
											dTmp = Val(Mid(strTmp, arFldDef(iCnt).fldOffset, arFldDef(iCnt).fldLen))
											If dTmp > 2147483647 Then
												LogMsg("*** Invalid value for I4: " & dTmp & " at rec# " & lCnt)
												ErrorCount = ErrorCount + 1
												'Else
												'   dbRs(arFldDef(iCnt).fldName).Value = dTmp
											End If
									End Select
								Case "I8"
									dTmp = Val(Mid(strTmp, arFldDef(iCnt).fldOffset, arFldDef(iCnt).fldLen))
								Case "T1" 'Suffix
									sTmp = Trim(Mid(strTmp, arFldDef(iCnt).fldOffset, arFldDef(iCnt).fldLen))
									iTmp = Val(sTmp)
								Case "T2" 'City field
									sTmp = Trim(Mid(strTmp, arFldDef(iCnt).fldOffset, arFldDef(iCnt).fldLen))
									iTmp = Val(sTmp)
							End Select
					End Select
					
					Select Case arFldDef(iCnt).fldName
						Case "APN"
							OutRecSML.APNS = sTmp
						Case "FMT_APN"
							OutRecSML.APND = sTmp
							'Case "CNTYCODE"
							'   OutRecSML.COID = sCnty
						Case "HO_FL"
							OutRecSML.HOEXE = sTmp
						Case "USE_CO"
							OutRecSML.USECODE = sTmp
						Case "STATUS"
							OutRecSML.Status2 = sTmp
						Case "TRA"
							OutRecSML.TRA = sTmp
						Case "NAME1"
							OutRecSML.NAME1 = sTmp
						Case "NAME2"
							OutRecSML.NAME2 = sTmp
						Case "GROSS_VAL"
							OutRecSML.GROSS = FormatData1(arFldDef(iCnt).fldLen, "R", dTmp)
					End Select
				Next 
				
				If bError = False Then
					OutRecSML.SADDR = SADDR
					OutRecSML.SCTYST = sCity
					OutRecSML.MADDR = MADDR
					OutRecSML.MCTYST = MCity
					OutRecSML.ADDRMCH = " "
					OutRecSML.HOMCH = " "
					'UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"'
					FilePut(OutFile, OutRecSML, counter)
				End If
				System.Windows.Forms.Application.DoEvents()
				If (lCnt Mod g_RecCount) = 0 Then
					counter = 0
					rc = CloseOutputFile()
					rc = OpenOutputFile(g_sOutputFile)
				End If
				If (lCnt Mod 10000) = 0 And g_bAutoLoad = False Then
					frmLoadDB.DefInstance.lblCounter.Text = VB6.Format(lCnt, "#,###,###,###")
					frmLoadDB.DefInstance.Label9.Text = CStr(TimeOfDay)
					If bStop Then
						If MsgBox("Are you sure you want to stop?", MsgBoxStyle.YesNo, "Stop Loading") = MsgBoxResult.Yes Then
							LogMsg("Stopped by user request at " & counter & " on " & g_sInputFile)
							rc = CloseOutputFile()
							Exit Do
						End If
					End If
				End If
			End If
		Loop 
		LogMsg("Complete " & g_sInputFile & " with total " & MaxCount & " records")
		If ErrorCount > 0 Then
			LogMsg("Complete " & g_sInputFile & " with total error " & ErrorCount & " records")
		End If
		
		fc.CloseFile()
		iRet = 0
		If g_bAutoLoad = False Then frmLoadDB.DefInstance.lblCounter.Text = CStr(lCnt)
		
loadDB_Error: 
		On Error Resume Next
		
		If iRet < 0 Then
			'UPGRADE_WARNING: Couldn't resolve default property of object sTemp. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
			LogMsg("Error loading data at record " & lCnt & ", field " & arFldDef(iCnt).fldName & ", value " & sTemp)
			ErrorCount = ErrorCount + 1
		Else
			LogMsg("Loading data complete on " & g_sInputFile)
		End If
		'UPGRADE_NOTE: Object fc may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1029"'
		fc = Nothing
	End Sub
	
	'Loading G01 file
	Public Sub loadG01(ByVal sCnty As String)
		Dim iCnt, iRet, fh, iTmp As Short
		Dim sql As String
		Dim strError As String
		Dim lRet, lTmp As Integer
		Dim dTmp As Double
		Dim sTemp As Object
		Dim strTmp, sTmp As String
		Dim Fmt As String
		Dim MaxCount As Integer
		
		Dim sApn As String
		Dim SADDR As String
		Dim sCity As String
		Dim MADDR As String
		Dim MCity As String
		
		Dim bError As Boolean
		Dim ErrorCount As Integer
		Dim SOutRec As SmlRec
		Dim lRecSize As Integer
		
		ErrorCount = 0
		iRet = -1
		On Error GoTo loadG01_Error
		
		LogMsg("Loop through " & InputFile)
		MaxCount = 0
		
		SOutRec.COID = sCnty
		SOutRec.ADDRMCH = " "
		SOutRec.HOMCH = " "
		
		fc = New clsBigFile
		fc.OpenFile(InputFile)
		
		Do While True
			MaxCount = MaxCount + 1
			
			lRet = fc.ReadRecs(g_iRecSize, strTmp)
			If lRet <> g_iRecSize Then
				LogMsg("End of file @" & lCnt)
				Exit Do
			End If
			bError = False
			If VB.Left(strTmp, 10) <> "9999999999" Then
				SADDR = ""
				sCity = ""
				MADDR = ""
				MCity = ""
				
				iRet = -2
				lCnt = lCnt + 1
				counter = counter + 1
				For iCnt = 0 To iNumFlds - 1
					sTmp = Trim(Mid(strTmp, arFldDef(iCnt).fldOffset, arFldDef(iCnt).fldLen))
					
					Select Case arFldDef(iCnt).fldName
						Case "APN"
							SOutRec.APNS = sTmp
						Case "FMT_APN"
							SOutRec.APND = sTmp
							'Case "CNTYCODE"
							'   sOutRec.COID = sCnty
						Case "HO_FL"
							If sTmp = "1" Then
								SOutRec.HOEXE = "Y"
							Else
								SOutRec.HOEXE = "N"
							End If
						Case "USE_CO"
							SOutRec.USECODE = sTmp
						Case "STATUS"
							SOutRec.Status2 = sTmp
						Case "TRA"
							SOutRec.TRA = sTmp
						Case "NAME1"
							SOutRec.NAME1 = sTmp
						Case "NAME2"
							SOutRec.NAME2 = sTmp
						Case "GROSS_VAL"
							SOutRec.GROSS = sTmp
						Case "S_ADDR_D"
							SOutRec.SADDR = sTmp
						Case "S_CTY_ST_D"
							SOutRec.SCTYST = sTmp
						Case "M_ADDR_D"
							SOutRec.MADDR = sTmp
						Case "M_CTY_ST_D"
							MCity = sTmp
						Case "M_ZIP"
							MCity = MCity & " " & sTmp
					End Select
				Next 
				
				SOutRec.MCTYST = MCity
				SOutRec.ADDRMCH = " "
				SOutRec.HOMCH = " "
				
				If g_bUseBig = False Then
					'UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"'
					FilePut(OutFile, SOutRec, counter)
					System.Windows.Forms.Application.DoEvents()
					If (lCnt Mod g_RecCount) = 0 Then
						counter = 0
						rc = CloseOutputFile()
						rc = OpenOutputFile(g_sOutputFile)
					End If
				Else
					'fcOut.WriteRec SOutRec, lRecSize
				End If
				
				If (lCnt Mod 10000) = 0 And g_bAutoLoad = False Then
					frmLoadDB.DefInstance.lblCounter.Text = VB6.Format(lCnt, "#,###,###,###")
					frmLoadDB.DefInstance.Label9.Text = CStr(TimeOfDay)
					If bStop Then
						If MsgBox("Are you sure you want to stop?", MsgBoxStyle.YesNo, "Stop Loading") = MsgBoxResult.Yes Then
							LogMsg("Stopped by user request at " & counter & " on " & g_sInputFile)
							rc = CloseOutputFile()
							Exit Do
						End If
					End If
				End If
			End If
		Loop 
		LogMsg("Complete " & g_sInputFile & " with total " & MaxCount & " records")
		If ErrorCount > 0 Then
			LogMsg("Complete " & g_sInputFile & " with total error " & ErrorCount & " records")
		End If
		
		fc.CloseFile()
		iRet = 0
		If g_bAutoLoad = False Then frmLoadDB.DefInstance.lblCounter.Text = CStr(lCnt)
		
loadG01_Error: 
		On Error Resume Next
		
		If iRet < 0 Then
			'UPGRADE_WARNING: Couldn't resolve default property of object sTemp. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
			LogMsg("Error loading data at record " & lCnt & ", field " & arFldDef(iCnt).fldName & ", value " & sTemp)
			ErrorCount = ErrorCount + 1
		Else
			LogMsg("Loading data complete on " & InputFile)
		End If
		'UPGRADE_NOTE: Object fc may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1029"'
		fc = Nothing
	End Sub
	
	Private Sub frmLoadDB_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		Call LoadCounty()
		lCnt = 0
		counter = 0
		txtRecSize.Text = CStr(g_iRecSize)
		txtCfgFile.Text = g_sFieldDef
		txtInputFile.Text = g_sInputFile
		txtOutputFile.Text = g_sOutputFile
		'UPGRADE_ISSUE: App property App.Revision was not upgraded. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2069"'
		Me.Text = "MergeSML ver. " & System.Diagnostics.FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly.Location).FileMajorPart & "." & System.Diagnostics.FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly.Location).FileMinorPart & "." & App.Revision
	End Sub
	
	'UPGRADE_WARNING: Event optCsv.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"'
	Private Sub optCsv_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optCsv.CheckedChanged
		If eventSender.Checked Then
			Call txtCounty_TextChanged(txtCounty, New System.EventArgs())
		End If
	End Sub
	
	'UPGRADE_WARNING: Event optFix.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"'
	Private Sub optFix_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optFix.CheckedChanged
		If eventSender.Checked Then
			Call txtCounty_TextChanged(txtCounty, New System.EventArgs())
		End If
	End Sub
	
	'UPGRADE_WARNING: Event txtCounty.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2075"'
	Private Sub txtCounty_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCounty.TextChanged
		Dim sTmp As String
		
		If cbReset.CheckState = System.Windows.Forms.CheckState.Unchecked Then
			sTmp = txtInputFile.Text
			
			If optFix.Checked = True Then
				sTmp = g_sInputPath & "\S" & txtCounty.Text & "_CD\RAW\S" & txtCounty.Text & ".G01"
				g_iInputType = R01_TYPE
			End If
			If optCsv.Checked = True Then
				sTmp = g_sInputPath & "\S" & txtCounty.Text & "_CD\S" & txtCounty.Text & "ASSR\S" & txtCounty.Text & ".CMP"
				g_iInputType = CMP_TYPE
			End If
			txtInputFile.Text = sTmp
			txtDatabase.Text = txtCounty.Text
		End If
		
	End Sub
	
	'Turn all button ON/OFF
	Public Sub switchButtons(ByRef bMode As Boolean)
		If bMode = False Then
			Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
		Else
			'UPGRADE_ISSUE: Unable to determine which constant to upgrade vbNormal to. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2049"'
			'UPGRADE_ISSUE: Form property frmLoadDB.MousePointer does not support custom mousepointers. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2036"'
			Me.Cursor = vbNormal
		End If
		
		cmdLoadData.Enabled = bMode
		cmdTruncate.Enabled = bMode
		cmdLoadCityTbl.Enabled = bMode
		cmdLoadSfxTbl.Enabled = bMode
	End Sub
End Class