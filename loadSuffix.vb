Option Strict Off
Option Explicit On
Module loadSuffix
	
	Public g_sSuffixes() As String
	Public g_iSuffixes As Short
	
	Public Function loadSuffixTbl(ByRef sSfxFile As String) As Short
		Dim sTmp As String
		Dim sSuffixTbl As String
		Dim fh As Short
		Dim iCnt As Short
		
		On Error GoTo loadSuffixTbl_Err
		fh = FreeFile
		sSuffixTbl = sSfxFile
		
		FileOpen(fh, sSuffixTbl, OpenMode.Input)
		sTmp = LineInput(fh)
		g_iSuffixes = CShort(sTmp)
		ReDim g_sSuffixes(g_iSuffixes)
		
		iCnt = 0
		'First entry is normally blank
		Do While Not EOF(fh)
			sTmp = LineInput(fh)
			g_sSuffixes(iCnt) = Trim(sTmp)
			iCnt = iCnt + 1
			If iCnt > g_iSuffixes Then
				MsgBox("Bad Suffix table.  Please check " & sSuffixTbl)
			End If
		Loop 
		FileClose(fh)
		fh = 0
		
		If iCnt < g_iSuffixes Then
			MsgBox("Suffix table count is unmatched.  Please check " & sSuffixTbl)
			loadSuffixTbl = 0
		Else
			loadSuffixTbl = 1
		End If
		
		Exit Function
		
loadSuffixTbl_Err: 
		If fh <> 0 Then FileClose(fh)
		MsgBox("Error loading Suffix table " & sSuffixTbl)
		loadSuffixTbl = 0
	End Function
	
	Public Function getSuffixName(ByRef iSuffixCode As Short) As String
		If iSuffixCode > 0 And iSuffixCode <= g_iSuffixes Then
			getSuffixName = g_sSuffixes(iSuffixCode)
		Else
			getSuffixName = ""
		End If
	End Function
End Module