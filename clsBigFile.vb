Option Strict Off
Option Explicit On
Friend Class clsBigFile
	
	Public Enum W32F_Errors
		W32F_UNKNOWN_ERROR = 45600
		W32F_FILE_ALREADY_OPEN
		W32F_PROBLEM_OPENING_FILE
		W32F_FILE_ALREADY_CLOSED
		W32F_Problem_seeking
	End Enum
	Private Structure TFileStat
		Dim dwFileAttributes As Integer
		Dim ftCreationTime As Integer
		Dim ftLastAccessTime As Integer
		Dim ftLastWriteTime As Integer
		Dim nFileSize As Double
	End Structure
	
	Private Const W32F_SOURCE As String = "Win32File Object"
	
	Private Const GENERIC_WRITE As Integer = &H40000000
	Private Const GENERIC_READ As Integer = &H80000000
	Private Const FILE_ATTRIBUTE_NORMAL As Short = &H80s
	Private Const CREATE_ALWAYS As Short = 2
	Private Const OPEN_ALWAYS As Short = 4
	Private Const INVALID_HANDLE_VALUE As Short = -1
	
	Private Const FILE_BEGIN As Short = 0
	Private Const FILE_CURRENT As Short = 1
	Private Const FILE_END As Short = 2
	
	Private Const FORMAT_MESSAGE_FROM_SYSTEM As Short = &H1000s
	
	'UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1016"'
	Private Declare Function FormatMessage Lib "kernel32"  Alias "FormatMessageA"(ByVal dwFlags As Integer, ByRef lpSource As Integer, ByVal dwMessageId As Integer, ByVal dwLanguageId As Integer, ByVal lpBuffer As String, ByVal nSize As Integer, ByRef Arguments As Any) As Integer
	
	'UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1016"'
	Private Declare Function ReadFile Lib "kernel32" (ByVal hFile As Integer, ByRef lpBuffer As Any, ByVal nNumberOfBytesToRead As Integer, ByRef lpNumberOfBytesRead As Integer, ByVal lpOverlapped As Integer) As Integer
	
	Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Integer) As Integer
	
	'UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1016"'
	Private Declare Function WriteFile Lib "kernel32" (ByVal hFile As Integer, ByRef lpBuffer As Any, ByVal nNumberOfBytesToWrite As Integer, ByRef lpNumberOfBytesWritten As Integer, ByVal lpOverlapped As Integer) As Integer
	
	Private Declare Function CreateFile Lib "kernel32"  Alias "CreateFileA"(ByVal lpFileName As String, ByVal dwDesiredAccess As Integer, ByVal dwShareMode As Integer, ByVal lpSecurityAttributes As Integer, ByVal dwCreationDisposition As Integer, ByVal dwFlagsAndAttributes As Integer, ByVal hTemplateFile As Integer) As Integer
	
	Private Declare Function SetFilePointer Lib "kernel32" (ByVal hFile As Integer, ByVal lDistanceToMove As Integer, ByRef lpDistanceToMoveHigh As Integer, ByVal dwMoveMethod As Integer) As Integer
	
	Private Declare Function FlushFileBuffers Lib "kernel32" (ByVal hFile As Integer) As Integer
	'UPGRADE_WARNING: Structure TFileStat may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1050"'
	Private Declare Function fileStat Lib "cddlib" (ByVal lpFileName As String, ByRef lpFileStat As TFileStat) As Integer
	'UPGRADE_NOTE: timeString was upgraded to timeString_Renamed. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1061"'
	Private Declare Sub timeString_Renamed Lib "cddlib" (ByVal lpTime As String, ByVal lTime As Integer)
	
	Private hFile As Integer
	Private sFName As String
	Private fAutoFlush As Boolean
	
	Public ReadOnly Property FileHandle() As Integer
		Get
			If hFile = INVALID_HANDLE_VALUE Then
				RaiseError(W32F_Errors.W32F_FILE_ALREADY_CLOSED)
			End If
			FileHandle = hFile
		End Get
	End Property
	
	Public ReadOnly Property fileName() As String
		Get
			If hFile = INVALID_HANDLE_VALUE Then
				RaiseError(W32F_Errors.W32F_FILE_ALREADY_CLOSED)
			End If
			fileName = sFName
		End Get
	End Property
	
	Public ReadOnly Property IsOpen() As Boolean
		Get
			IsOpen = hFile <> INVALID_HANDLE_VALUE
		End Get
	End Property
	
	
	Public Property AutoFlush() As Boolean
		Get
			If hFile = INVALID_HANDLE_VALUE Then
				RaiseError(W32F_Errors.W32F_FILE_ALREADY_CLOSED)
			End If
			AutoFlush = fAutoFlush
		End Get
		Set(ByVal Value As Boolean)
			If hFile = INVALID_HANDLE_VALUE Then
				RaiseError(W32F_Errors.W32F_FILE_ALREADY_CLOSED)
			End If
			fAutoFlush = Value
		End Set
	End Property
	
	Public Sub OpenFile(ByVal sFileName As String)
		If hFile <> INVALID_HANDLE_VALUE Then
			RaiseError(W32F_Errors.W32F_FILE_ALREADY_OPEN, sFName)
		End If
		hFile = CreateFile(sFileName, GENERIC_WRITE Or GENERIC_READ, 0, 0, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0)
		If hFile = INVALID_HANDLE_VALUE Then
			RaiseError(W32F_Errors.W32F_PROBLEM_OPENING_FILE, sFileName)
		End If
		sFName = sFileName
	End Sub
	
	Public Sub CloseFile()
		If hFile = INVALID_HANDLE_VALUE Then
			RaiseError(W32F_Errors.W32F_FILE_ALREADY_CLOSED)
		End If
		CloseHandle(hFile)
		sFName = ""
		fAutoFlush = False
		hFile = INVALID_HANDLE_VALUE
	End Sub
	
	Public Function ReadBytes(ByVal ByteCount As Integer) As Object
		Dim BytesRead As Integer
		Dim Bytes() As Byte
		If hFile = INVALID_HANDLE_VALUE Then
			RaiseError(W32F_Errors.W32F_FILE_ALREADY_CLOSED)
		End If
		ReDim Bytes(ByteCount - 1)
		ReadFile(hFile, Bytes(0), ByteCount, BytesRead, 0)
		'UPGRADE_WARNING: Couldn't resolve default property of object ReadBytes. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
		ReadBytes = VB6.CopyArray(Bytes)
	End Function
	
	Public Function ReadRecs(ByVal ByteCount As Integer, ByRef strBuf As String) As Integer
		Dim BytesRead As Integer
		Dim Bytes() As Byte
		Dim bRet As Boolean
		Dim strTmp As String
		
		If hFile = INVALID_HANDLE_VALUE Then
			RaiseError(W32F_Errors.W32F_FILE_ALREADY_CLOSED)
		End If
		ReDim Bytes(ByteCount - 1)
		bRet = ReadFile(hFile, Bytes(0), ByteCount, BytesRead, 0)
		'UPGRADE_ISSUE: Constant vbUnicode was not upgraded. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup2070"'
		strTmp = StrConv(System.Text.UnicodeEncoding.Unicode.GetString(Bytes), vbUnicode)
		strBuf = strTmp
		ReadRecs = BytesRead
	End Function
	
	Public Sub WriteBytes(ByRef DataBytes() As Byte)
		Dim BytesToWrite, fSuccess, BytesWritten As Integer
		If hFile = INVALID_HANDLE_VALUE Then
			RaiseError(W32F_Errors.W32F_FILE_ALREADY_CLOSED)
		End If
		BytesToWrite = UBound(DataBytes) - LBound(DataBytes) + 1
		fSuccess = WriteFile(hFile, DataBytes(LBound(DataBytes)), BytesToWrite, BytesWritten, 0)
		If fAutoFlush Then Flush()
	End Sub
	
	Public Sub WriteRec(ByRef OutRec() As Byte, ByRef lRecLen As Integer)
		Dim fSuccess, BytesWritten As Integer
		If hFile = INVALID_HANDLE_VALUE Then
			RaiseError(W32F_Errors.W32F_FILE_ALREADY_CLOSED)
		End If
		fSuccess = WriteFile(hFile, OutRec(0), lRecLen, BytesWritten, 0)
		If fAutoFlush Then Flush()
	End Sub
	
	Public Sub Flush()
		If hFile = INVALID_HANDLE_VALUE Then
			RaiseError(W32F_Errors.W32F_FILE_ALREADY_CLOSED)
		End If
		FlushFileBuffers(hFile)
	End Sub
	
	Public Sub SeekAbsolute(ByVal HighPos As Integer, ByVal LowPos As Integer)
		If hFile = INVALID_HANDLE_VALUE Then
			RaiseError(W32F_Errors.W32F_FILE_ALREADY_CLOSED)
		End If
		LowPos = SetFilePointer(hFile, LowPos, HighPos, FILE_BEGIN)
	End Sub
	
	Public Sub SeekRelative(ByVal Offset As Integer)
		Dim TempLow, TempErr As Integer
		If hFile = INVALID_HANDLE_VALUE Then
			RaiseError(W32F_Errors.W32F_FILE_ALREADY_CLOSED)
		End If
		TempLow = SetFilePointer(hFile, Offset, 0, FILE_CURRENT)
		If TempLow = -1 Then
			TempErr = Err.LastDllError
			If TempErr Then
				RaiseError(W32F_Errors.W32F_Problem_seeking, "Error " & TempErr & "." & vbCrLf & CStr(TempErr))
			End If
		End If
	End Sub
	
	Public Function getFilePointer() As Double
		Dim TempErr As Integer
		Dim TempDbl As Double
		
		If hFile = INVALID_HANDLE_VALUE Then
			RaiseError(W32F_Errors.W32F_FILE_ALREADY_CLOSED)
		End If
		TempDbl = SetFilePointer(hFile, 0, 0, FILE_CURRENT)
		If TempDbl = -1 Then
			TempErr = Err.LastDllError
			If TempErr Then
				RaiseError(W32F_Errors.W32F_Problem_seeking, "Error " & TempErr & "." & vbCrLf & CStr(TempErr))
			End If
		End If
		getFilePointer = TempDbl
	End Function
	
	'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1061"'
	Private Sub Class_Initialize_Renamed()
		hFile = INVALID_HANDLE_VALUE
	End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
	End Sub
	
	'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1061"'
	Private Sub Class_Terminate_Renamed()
		If hFile <> INVALID_HANDLE_VALUE Then CloseHandle(hFile)
	End Sub
	Protected Overrides Sub Finalize()
		Class_Terminate_Renamed()
		MyBase.Finalize()
	End Sub
	
	Private Sub RaiseError(ByVal ErrorCode As W32F_Errors, Optional ByRef sExtra As Object = Nothing)
		Dim Win32Err As Integer
		Dim Win32Text As String
		Win32Err = Err.LastDllError
		If Win32Err Then
			Win32Text = vbCrLf & "Error " & Win32Err & vbCrLf & DecodeAPIErrors(Win32Err)
		End If
		
		Select Case ErrorCode
			Case W32F_Errors.W32F_FILE_ALREADY_OPEN
				'UPGRADE_WARNING: Couldn't resolve default property of object sExtra. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
				Err.Raise(W32F_Errors.W32F_FILE_ALREADY_OPEN, W32F_SOURCE, "The file '" & sExtra & "' is already open." & Win32Text)
			Case W32F_Errors.W32F_PROBLEM_OPENING_FILE
				'UPGRADE_WARNING: Couldn't resolve default property of object sExtra. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
				Err.Raise(W32F_Errors.W32F_PROBLEM_OPENING_FILE, W32F_SOURCE, "Error opening '" & sExtra & "'." & Win32Text)
			Case W32F_Errors.W32F_FILE_ALREADY_CLOSED
				Err.Raise(W32F_Errors.W32F_FILE_ALREADY_CLOSED, W32F_SOURCE, "There is no open file.")
			Case W32F_Errors.W32F_Problem_seeking
				'UPGRADE_WARNING: Couldn't resolve default property of object sExtra. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
				Err.Raise(W32F_Errors.W32F_Problem_seeking, W32F_SOURCE, "Seek Error." & vbCrLf & sExtra)
			Case Else
				Err.Raise(W32F_Errors.W32F_UNKNOWN_ERROR, W32F_SOURCE, "Unknown error." & Win32Text)
		End Select
	End Sub
	
	Private Function DecodeAPIErrors(ByVal ErrorCode As Integer) As String
		Dim sMessage As String
		Dim MessageLength As Integer
		sMessage = Space(256)
		MessageLength = FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, 0, ErrorCode, 0, sMessage, 256, 0)
		If MessageLength > 0 Then
			DecodeAPIErrors = Left(sMessage, MessageLength)
		Else
			DecodeAPIErrors = "Unknown Error."
		End If
	End Function
	
	Public Function getFileLength(ByVal strFilename As String) As Double
		Dim myStat As TFileStat
		Dim dRet As Double
		
		myStat.nFileSize = 0
		dRet = fileStat(strFilename, myStat)
		If dRet = -1 Then
			getFileLength = 0
		Else
			getFileLength = myStat.nFileSize
		End If
	End Function
	
	Public Function getFileCTime(ByVal strFilename As String) As String
		Dim myStat As TFileStat
		Dim dRet As Double
		Dim strTmp As New VB6.FixedLengthString(32)
		
		dRet = fileStat(strFilename, myStat)
		If dRet = -1 Then
			getFileCTime = ""
		Else
			timeString_Renamed(strTmp.Value, myStat.ftCreationTime)
			getFileCTime = strTmp.Value
		End If
	End Function
	
	Public Function getFileMTime(ByVal strFilename As String) As String
		Dim myStat As TFileStat
		Dim dRet As Double
		Dim strTmp As New VB6.FixedLengthString(32)
		
		dRet = fileStat(strFilename, myStat)
		If dRet = -1 Then
			getFileMTime = ""
		Else
			timeString_Renamed(strTmp.Value, myStat.ftLastWriteTime)
			getFileMTime = strTmp.Value
		End If
	End Function
	
	Public Function getFileATime(ByVal strFilename As String) As String
		Dim myStat As TFileStat
		Dim dRet As Double
		Dim strTmp As New VB6.FixedLengthString(32)
		
		dRet = fileStat(strFilename, myStat)
		If dRet = -1 Then
			getFileATime = ""
		Else
			timeString_Renamed(strTmp.Value, myStat.ftLastAccessTime)
			getFileATime = strTmp.Value
		End If
	End Function
End Class