Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Module modMain
	
	Public Const R01_TYPE As Short = 1
	Public Const CMP_TYPE As Short = 2
	
	Public dbConn As ADODB.Connection
	Public dbRs As ADODB.Recordset
	Public g_sServer As String
	Public g_sDatabase As String
	Public g_sProvider As String
	Public g_sInputFile As String
	Public g_sFieldDef As String
	Public g_sCityPath As String
	Public g_sInputPath As String
	Public g_sFixExt As String
	Public g_sSfxTable As String
	Public g_sCntyTable As String
	Public g_sTable As String
	
	Public g_sOutFieldDef As String
	Public g_sOutputFile As String
	Public g_sRecSize As Short
	
	Public g_RecCount As Integer
	
	Public g_bAutoLoad As Boolean
	Public g_bUseBig As Boolean
	Public g_iRecSize As Short
	Public g_iInputType As Short
	
	Public lCnt As Integer
	Public bLoading As Boolean
	Public bStop As Boolean
	
	Public fc As clsBigFile
	Public fcOut As clsBigFile
	
	Public Structure SOutRec
		<VBFixedString(14),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr,SizeConst:=14)> Public APNS As String
		<VBFixedString(17),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr,SizeConst:=17)> Public APND As String
		<VBFixedString(3),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr,SizeConst:=3)> Public COID As String
		<VBFixedString(1),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr,SizeConst:=1)> Public Status2 As String
		<VBFixedString(1),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr,SizeConst:=1)> Public HOEXE As String
		<VBFixedString(6),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr,SizeConst:=6)> Public USECODE As String
		<VBFixedString(8),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr,SizeConst:=8)> Public TRA As String
		<VBFixedString(52),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr,SizeConst:=52)> Public NAME1 As String
		<VBFixedString(26),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr,SizeConst:=26)> Public NAME2 As String
		<VBFixedString(10),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr,SizeConst:=10)> Public GROSS As String
		<VBFixedString(52),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr,SizeConst:=52)> Public SADDR As String
		<VBFixedString(38),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr,SizeConst:=38)> Public SCTYST As String
		<VBFixedString(52),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr,SizeConst:=52)> Public MADDR As String
		<VBFixedString(31),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr,SizeConst:=31)> Public MCTYST As String
		<VBFixedString(1),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr,SizeConst:=1)> Public ADDRMCH As String
		<VBFixedString(1),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr,SizeConst:=1)> Public HOMCH As String
	End Structure
	
	Public Structure SmlRec
		<VBFixedString(14),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr,SizeConst:=14)> Public APNS As String
		<VBFixedString(17),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr,SizeConst:=17)> Public APND As String
		<VBFixedString(3),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr,SizeConst:=3)> Public COID As String
		<VBFixedString(1),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr,SizeConst:=1)> Public Status2 As String
		<VBFixedString(1),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr,SizeConst:=1)> Public HOEXE As String
		<VBFixedString(6),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr,SizeConst:=6)> Public USECODE As String
		<VBFixedString(8),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr,SizeConst:=8)> Public TRA As String
		<VBFixedString(52),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr,SizeConst:=52)> Public NAME1 As String
		<VBFixedString(26),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr,SizeConst:=26)> Public NAME2 As String
		<VBFixedString(10),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr,SizeConst:=10)> Public GROSS As String
		<VBFixedString(52),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr,SizeConst:=52)> Public SADDR As String
		<VBFixedString(38),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr,SizeConst:=38)> Public SCTYST As String
		<VBFixedString(52),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr,SizeConst:=52)> Public MADDR As String
		<VBFixedString(31),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr,SizeConst:=31)> Public MCTYST As String
		<VBFixedString(1),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr,SizeConst:=1)> Public ADDRMCH As String
		<VBFixedString(1),System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr,SizeConst:=1)> Public HOMCH As String
	End Structure
	
	Public OutRecSML As SOutRec
	Public OutFile As Short
	Public InputFile As String
	Public rc As Integer
	'Public Sub LogMsg(ByVal strMsg As String)
	'   Dim ff As Integer
	'
	'   On Error GoTo ErrorHandler
	'   ff = FreeFile
	'
	'   Open g_sLogFile For Append As ff
	'   Print #ff, Now & vbTab & strMsg
	'   Close ff
	'
	'ErrorHandler:
	'End Sub
	
	Public Sub clearTable(ByRef sTable As String)
		Dim sqlCmd As New ADODB.Command
		
		On Error GoTo clearTable_Error
		
		LogMsg("Open db connection: " & g_sProvider)
		dbConn = New ADODB.Connection
		dbConn.ConnectionString = "Provider=" & g_sProvider
		dbConn.Open()
		
		LogMsg("Empty table " & sTable & " from " & g_sDatabase)
		sqlCmd.CommandText = "TRUNCATE TABLE " & sTable
		sqlCmd.let_ActiveConnection(dbConn)
		sqlCmd.Execute()
		
		GoTo clearTable_Exit
		
clearTable_Error: 
		LogMsg("Error clearing table: " & Err.Description)
		
clearTable_Exit: 
		'UPGRADE_NOTE: Object sqlCmd may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1029"'
		sqlCmd = Nothing
		If Not dbConn Is Nothing Then
			If dbConn.State = ADODB.ObjectStateEnum.adStateOpen Then
				dbConn.Close()
			End If
		End If
		'UPGRADE_NOTE: Object dbConn may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1029"'
		dbConn = Nothing
	End Sub
	
	
	'UPGRADE_WARNING: Application will terminate when Sub Main() finishes. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1047"'
	Public Sub Main()
		'**************************************************************************
		Dim sTemp, sCmd As String
		Dim bRet As Boolean
		
		'Retrieve command line
		sCmd = VB.Command()
		
		'Load INI file
		If Not InitSettings() Then
			MsgBox("Error initializing loadDB: " & Err.Description)
			Exit Sub
		End If
		LogMsg("Start MergeSML")
		g_bAutoLoad = False
		g_bUseBig = False
		frmLoadDB.DefInstance.Show()
	End Sub
	
	Private Function InitSettings() As Boolean
		Dim sTmp As String
		
		On Error Resume Next
		
		InitSettings = True
		sTmp = GetIniData(VB6.GetExeName(), "System", "LogPath")
		g_logFile = sTmp & "\MergeSML.log"
		g_sInputPath = GetIniData(VB6.GetExeName(), "System", "InputPath")
		g_sCityPath = GetIniData(VB6.GetExeName(), "System", "CityPath")
		g_sSfxTable = GetIniData(VB6.GetExeName(), "System", "SuffixTbl")
		g_sCntyTable = GetIniData(VB6.GetExeName(), "System", "CountyTbl")
		
		g_sInputFile = GetIniData(VB6.GetExeName(), "Data", "RawData")
		g_sFieldDef = GetIniData(VB6.GetExeName(), "Data", "FieldDef")
		g_iRecSize = CShort(GetIniData(VB6.GetExeName(), "Data", "RecSize"))
		g_sFixExt = GetIniData(VB6.GetExeName(), "Data", "FixExt")
		
		g_sOutputFile = GetIniData(VB6.GetExeName(), "output", "RawData")
		g_sOutFieldDef = GetIniData(VB6.GetExeName(), "output", "FieldDef")
		g_sRecSize = CShort(GetIniData(VB6.GetExeName(), "output", "RecSize"))
		g_RecCount = CInt(GetIniData(VB6.GetExeName(), "System", "RecCount"))
		
		'Init county table
		'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1041"'
		If Dir(g_sCntyTable) <> "" Then
			initCntyInfo(g_sCntyTable)
		End If
	End Function
End Module